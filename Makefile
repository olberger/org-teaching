DECKTAPESHCMD=bin/decktape.sh
# DECKTAPESHOPTS="-s 1280x960"
DECKTAPESHOPTS=

GENERATED_DOCS=slides.html slides.pdf handbook.pdf teacher-handbook.pdf lesson.html
GENERATED_MINIMALS=minimal.html minimal.pdf

all:  README.html docs minimal

clean:
	$(RM) README.html $(GENERATED_DOCS) $(GENERATED_MINIMALS)

# Just the README in HTML
README.html:	README.org
	./docker/docker-org-teaching-export org-html-export-to-html $<

# Somewhat detailed docs
docs:	$(GENERATED_DOCS)


slides.html:	slides.org lesson.org
	./docker/docker-org-teaching-export org-reveal-export-to-html slides.org

slides.pdf: slides.html
	$(DECKTAPESHCMD) $(DECKTAPESHOPTS) slides.html slides.pdf

handbook.pdf:	handbook.org lesson.org headers-students.tex
	./docker/docker-org-teaching-export org-latex-export-to-pdf handbook.org

teacher-handbook.pdf: teacher-handbook.org lesson.org headers-teachers.tex
	./docker/docker-org-teaching-export org-latex-export-to-pdf teacher-handbook.org

# Just the README in HTML
lesson.html:	lesson.org
	./docker/docker-org-teaching-export org-html-export-to-html $<

# Minimal files to diminish complexity for beginners
minimal:	$(GENERATED_MINIMALS)

minimal.html:	minimal.org minimal-lesson.org
	./docker/docker-org-teaching-export org-reveal-export-to-html minimal.org

minimal.pdf: minimal.html
	$(DECKTAPESHCMD) $(DECKTAPESHOPTS) minimal.html minimal.pdf


org-reveal_Readme.html:	elisp/org-reveal/Readme.org
	./docker/docker-org-teaching-export org-reveal-export-to-html elisp/org-reveal/Readme.org
	cp elisp/org-reveal/Readme.html org-reveal_Readme.html

