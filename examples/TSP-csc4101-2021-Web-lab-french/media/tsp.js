/* Ajout d'une icône intercalée entre les divs et les ps des blocs
 * spéciaux attention et remarque permet de gérer les blocs spéciaux
 * org-mode BEGIN_attention / END_attention qui génèrent des
 * <div class="attention"><p>...
 */
function changeattention() {

    var attentiondivs = document.getElementsByClassName('attention');
    for (var i = 0; i < attentiondivs.length; ++i) {
        attentiondivs[i].innerHTML = '<i class="atticon"></i>' + attentiondivs[i].innerHTML;
    }
    var remarkdivs = document.getElementsByClassName('remarque');
    for (var i = 0; i < remarkdivs.length; ++i) {
        remarkdivs[i].innerHTML = '<i class="remicon"></i>' + remarkdivs[i].innerHTML;
    }

}

document.addEventListener('DOMContentLoaded', changeattention);

